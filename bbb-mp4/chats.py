
import csv
import xml.etree.ElementTree as ET
import sys




meetingid=sys.argv[1]
filename="/var/bigbluebutton/recording/raw/"+meetingid+"/events.xml"
savefile="/var/www/bigbluebutton-default/chats/"+meetingid+"_chat.txt"

file1 = open(savefile,"w")

mytree = ET.parse(filename).getroot()
for i in mytree.findall("event"):
	if(i.get('module')=='CHAT' and i.get('eventname')=='PublicChatEvent'):
		text=i.find('sender').text+": "+i.find('message').text+ "\n"
		file1.write(text)
		file1.write("\n\n")


file1.close()
