import csv
import xml.etree.ElementTree as ET
import sys




meetingid=sys.argv[1]
filename="/var/bigbluebutton/recording/raw/"+meetingid+"/events.xml"
savefile="/var/www/bigbluebutton-default/Attendance/"+meetingid+"_attend.txt"

#filename="events1.xml"
attendee={}
external_map={}
file1 = open(savefile,"w")
#print("here")
mytree = ET.parse(filename).getroot()
for i in mytree.findall("event"):
	if(i.get('module')=='PARTICIPANT' and i.get('eventname')=='ParticipantJoinEvent'):
		name=i.find('name').text
		external_id=i.find('userId').text
		if(attendee.has_key(name)):
			attendee[name]=int(attendee[name])-int(i.find('timestampUTC').text)
		else:
			attendee[name]=-int(i.find('timestampUTC').text)
		external_map[external_id]=name
	if(i.get('module')=='PARTICIPANT' and i.get('eventname')=='ParticipantLeftEvent'):
		id=i.find('userId').text
		attendee[external_map[id]]=int(attendee[external_map[id]])+int(i.find('timestampUTC').text)
		del external_map[id]

	if(i.get('module')=='PARTICIPANT' and i.get('eventname')=='EndAndKickAllEvent'):
		for id  in external_map:
			#print(id)
			attendee[external_map[id]]=int(attendee[external_map[id]])+int(i.find('timestampUTC').text)
			#del external_map[id]


text="Name"+"  "+" Active time"
file1.write(text)
file1.write("\n\n")
for name in attendee:
	text=(name+": "+ str(round(attendee[name]/(60000.0),2)) + " Minutes")
	#print("\n")
	file1.write(text)
	file1.write("\n\n")
	

	



